const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const newinformaticsBuilding = await Building.findById('621bb263d29401f03b00863a')
  const room = await Room.findById('621bb263d29401f03b00863f')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newinformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newinformaticsBuilding
  newinformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newinformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
